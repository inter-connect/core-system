'use strict';

/**
 * @ngdoc overview
 * @name coreSystemApp
 * @description
 * # coreSystemApp
 *
 * Main module of the application.
 */
angular
  .module('coreSystemApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
