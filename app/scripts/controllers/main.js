'use strict';

/**
 * @ngdoc function
 * @name coreSystemApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the coreSystemApp
 */
angular.module('coreSystemApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
